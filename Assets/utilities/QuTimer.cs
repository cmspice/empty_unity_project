using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class QuTimer{
	float mStart = 0,mCurrent,mTarget;
    bool mLooping;
    public bool Looping{ 
        get{ return mLooping; } 
        set{ 
            mLooping = value;
            while (mCurrent > mTarget)
                mCurrent -= mTarget;
        } 
    }
    //private bool mPaused = false;
    public QuTimer(float aCurrent = 0, float aTarget = 0)
    {
        mCurrent = aCurrent;
        mTarget = aTarget;
    }
	public float getTarget()
	{
		return mTarget;
	}
    public float getCurrent()
    {
        return mCurrent;
    }
	public void update(float delta)
	{
		mCurrent+=delta;
        if (Looping)
            while (mCurrent > mTarget)
                mCurrent -= mTarget;
	}
    public void reset()
	{
		mCurrent = mStart;
	}

    public bool isInitialized()
	{
		return mTarget != mStart;
	}
	
	public void deinitialize()
	{
		mTarget = mStart;
	}
	
    public void setTarget(float aTarget)
	{
		mTarget = aTarget;
	}
    public void setTargetAndReset(float aTarget)
	{
		setTarget(aTarget);
		reset();
	}
    public void expire()
	{
		mCurrent = mTarget;
	}
	//timer expires when mCurrent is >= mTarget
    public bool isExpired()
	{
		return mCurrent >= mTarget;
	}
    public float getTimeSinceStart()
	{
		return mCurrent-mStart;
	}
    public float getTimeSinceExpired()
    {
        return mCurrent - mTarget;
    }
    public float getLinear()
	{
		if(isExpired())
			return 1;
		return (float)(mCurrent-mStart)/(float)(mTarget-mStart);
	}
    public float getLinear(float l, float r)
	{
		return getLinear()*(r-l)+l;
	}
    public float getSquareRoot()
	{
		return Mathf.Sqrt(getLinear());
	}
	public float getOverShot()
	{
		return Mathf.Sin(getSquareRoot()*(Mathf.PI*0.5f+0.15f));
	}
    public float getSquare()
	{
		float r = getLinear();
		return r*r;
	}
    public float getCubed(float l = 0, float r = 1)
	{
		float v = getLinear();
		return v*v*v*(r-l)+l;
	}
	//0 and 0 and 1, max at 0.5
    public float getUpsidedownParabola()
	{
		float x = getLinear();
		return (float)(-(x-0.5)*(x-0.5) + 0.25)*4;
	}
	//1 at 0 and 1, 0 at 0.5
    public float getParabola(float l = 0, float r = 1)
	{
		return (1-getUpsidedownParabola())*(r-l)+l;
	}
	
}
